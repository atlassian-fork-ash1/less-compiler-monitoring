package com.atlassian.plugins.less.monitoring;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */
@Path("/stats")
public class LessCompilationRestResource {
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMessage(@QueryParam("key") String key) {
        return Response.ok(LessCompilationStore.getCompilations()).build();
    }
}
