package com.atlassian.plugins.less.monitoring;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.lesscss.spi.LessCssCompilationEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LessCssCompilationListener implements InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(LessCssCompilationListener.class);

    @JiraImport
    private final EventPublisher eventPublisher;

    @Autowired
    public LessCssCompilationListener(@JiraImport EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void afterPropertiesSet() {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() {
        eventPublisher.unregister(this);
    }

    @EventListener
    public void onIssueEvent(LessCssCompilationEvent compilationEvent) {
        long computations = LessCompilationStore.register(compilationEvent);
        log.trace("Resource '{}' - '{}' compilation", compilationEvent.getLessResourceUri(), computations);
    }
}
